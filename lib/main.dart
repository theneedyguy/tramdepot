import 'dart:async';
import 'dart:convert';
import "dart:math";
 
import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';
 
import 'package:http/http.dart' as http;


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Altes Tramdepot",
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.green,
        primaryColor: Color.fromRGBO(0, 33, 0, 1),
        cardColor: Color.fromRGBO(10, 66, 10, 1),
        fontFamily: "DroidSerif",

      ),
      home: MyHomePage(title: 'Altes Tramdepot'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String url = "https://api.purpl3.net/tramdepot/v1/beers.json";
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  List data;
  bool progressActive;
  Future<List> makeRequest() async {
    refreshKey.currentState?.show();
    progressActive = true;
    final response = await http.get(Uri.encodeFull(url), headers: {"Accept": "application/json"});
    setState(() {
      final extractdata = jsonDecode(response.body);
      progressActive = false;
      data = extractdata["beers"];
      return data;
    });
    return data;
  }


 @override
  void initState() {
    this.makeRequest();
  }


  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      backgroundColor:  Color.fromRGBO(0, 33, 0, 1),
      appBar: AppBar(
        
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Container(
          padding: EdgeInsets.only(top: 0),
          child: Text(widget.title, style: TextStyle(fontSize: 25)),
        )
        
      ),
        body: progressActive == true ? 
        // true (Is loading)
        Center(
          child: new CircularProgressIndicator() 
        )
        // false (is not loading)
        : new RefreshIndicator(
          onRefresh: makeRequest,
          key: refreshKey,
          child: 
         new ListView.builder(
            shrinkWrap: false,
            itemCount: data == null ? 0 : data.length,
            itemBuilder: (BuildContext context, i) {
              return Card(
               child: InkWell(onTap: (){Navigator.push(context, new MaterialPageRoute(builder: (BuildContext context) => new SecondPage(data[i])));},
                  child: Container(
                    child: ListTile(
                      contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                      leading: Container(
                        padding: EdgeInsets.only(right: 7),
                        decoration: new BoxDecoration(border: new Border(right: new BorderSide(width: 1.0, color: Colors.white24))),
                        child: CircularPercentIndicator(
                          radius: 70,
                          lineWidth: 8,
                          animationDuration: 1500,
                          circularStrokeCap: CircularStrokeCap.round,
                          percent: (double.parse(data[i]["litres_left"].toString().split(" ")[0])) / 1600,
                          progressColor: Colors.yellow,
                          animation: true,
                          center: Container(
                            child: new Text(data[i]["litres_left"], textAlign: TextAlign.center, style: TextStyle(fontFamily: "Roboto", color: Colors.white, fontSize: 14)),
                            width: 40,
                          ), 
                        ),
                      ),
                      title: Container(
                        width: 140,
                        margin: EdgeInsets.only(top: 3),
                        child: Text(data[i]["title"], style: TextStyle(color: Colors.white,height: 1.0, fontFamily: 'DroidSerif', fontSize: 25)),
                      ),                   
                      subtitle: Row(            
                        children: <Widget>[
                        
                          Container(
                            width: 150,
                            child:  new Text(data[i]["details"].toString().replaceAll("/", ""), 
                              overflow: TextOverflow.clip, 
                              style: TextStyle(fontFamily: "Roboto", color: Colors.white, fontSize: 15)),
                          ) 
                        ],
                      ),
                        trailing: Icon(Icons.keyboard_arrow_right, color: Colors.white, size: 30),
                    ), 
                  ),
                ), 
                elevation: 7.0,
                clipBehavior: Clip.antiAlias,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7)),
                margin: new EdgeInsets.symmetric(horizontal: 5.0, vertical: 7.0),
              );
              
/*
               return new ListTile(

                title: new Text(data[i]["title"]),
                subtitle: new Text(data[i]["description"]),
                leading: new CircleAvatar(
                  backgroundImage:
                  null
                      //new NetworkImage(data[i]["picture"]["thumbnail"]),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (BuildContext context) =>
                              new SecondPage(data[i])));
                },
              );
              */
          }
        )
      )
    );
  }
}

class SecondPage extends StatelessWidget {
  SecondPage(this.data);
  var list = ['alc-1.jpg','alc-2.jpg', 'alc-3.jpg', 'alc-4.jpg','alc-5.jpg','alc-6.jpg','alc-7.jpg'];
 
  final data;
  @override
  Widget build(BuildContext context) {

    final topContentLS = Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 10),
          height: MediaQuery.of(context).size.height * 0.38,
          decoration: new BoxDecoration(
            image: new DecorationImage(
              
              image: new AssetImage("assets/" + (list..shuffle()).first),
              fit: BoxFit.cover 
            )
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.38,
          padding: EdgeInsets.only(top:40),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Color.fromRGBO(10, 66, 10, .9)
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 20),
                  child: Text(data["title"], style: TextStyle(fontFamily: "DroidSerif", color: Colors.white, fontSize: 25)),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    LinearPercentIndicator(
                    width: MediaQuery.of(context).size.width * 0.5,
                    animation: true,
                    leading: Text(
                      data["litres_left"],
                      style: TextStyle(fontFamily: "Roboto", color: Colors.white)
                    ),
                    animationDuration: 1500,
                    progressColor: Colors.yellow,
                    percent: (double.parse(data["litres_left"].toString().split(" ")[0])) / 1600,
                  ),
                ],
                ),
               

              ],
            )
          ),
        ),
        Positioned(
          left: 8,
          top: 40,
          child: InkWell(
            onTap: (){Navigator.pop(context);},
            child: Icon(Icons.arrow_back, color: Colors.white, size: 35)),
        )
      ]);




    final topContent = Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 10),
          height: MediaQuery.of(context).size.height * 0.379,
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: new AssetImage("assets/" + (list..shuffle()).first),
              fit: BoxFit.cover
            )
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.38,
          padding: EdgeInsets.all(0),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Color.fromRGBO(10, 66, 10, .9)
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CircularPercentIndicator(
                  radius: 120,
                  animation: true,
                  animationDuration: 1500,
                  circularStrokeCap: CircularStrokeCap.round,
                  lineWidth: 5,
                  center: Container(
                    child: new Text(data["litres_left"], textAlign: TextAlign.center, style: TextStyle(fontFamily: "Roboto", color: Colors.white, fontSize: 19)),
                    width: 50,
                  ), 
                  progressColor: Colors.yellow,
                  percent: (double.parse(data["litres_left"].toString().split(" ")[0])) / 1600,
                ),
                Container(
                  padding: EdgeInsets.only(top: 10),
                  child: Text(data["title"], style: TextStyle(fontFamily: "DroidSerif", color: Colors.white, fontSize: 30)),
                ),
              ],
            )
          ),
        ),
        Positioned(
          left: 8,
          top: 40,
          child: InkWell(
            onTap: (){Navigator.pop(context);},
            child: Icon(Icons.arrow_back, color: Colors.white, size: 35)),
        )
      ]);

      final bottomContentText = Text(
        data["description"],
        style: TextStyle(fontSize: 22, fontFamily: "Roboto", color: Colors.white),
      );
        final bottomContentTextDetail = Text(
        data["details"],
        style: TextStyle(fontSize: 15, fontFamily: "Roboto", color: Colors.white54)
      );


      final bottomContent = Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(15),
         // child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                bottomContentText, bottomContentTextDetail
              ],
            ),
         // ),
      );




    // Return final view
    return Scaffold(
      backgroundColor:  Color.fromRGBO(0, 33, 0, 1),
      body: OrientationBuilder(
        builder: (context, orientation) {
            if (orientation == Orientation.portrait) {
              return Column(
                children: <Widget>[
                  topContent, bottomContent
                ],
              );
            }else{
              return Column(
                children: <Widget>[
                  topContentLS, bottomContent
                ],
              );
            }
        },
      )

    );
  }
}